# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.2.2a] - 2023-11-08

### Fixed

- Year is an FK now, not an int

## [0.2.0a] - yyyy-mm-dd

### Added

- discord notify buttons, on santa allocations and outstanding gifts

### Changed

- Years are now rendered as sub templates, no idea why i didnt do this earlier
- real discord DM templates, nothing fancy.

### Fixed

## [0.1.1a] - 2023-10-31

### Fixed

- remove unneeded related_names to reduce conflicts

## [0.1.0a] - 2023-10-31

Initial Minimum Viable Product
